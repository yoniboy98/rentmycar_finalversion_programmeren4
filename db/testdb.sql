CREATE DATABASE `test` ;
CREATE TABLE `cars` (
  `carid` int(11) NOT NULL AUTO_INCREMENT,
  `car name` varchar(100) NOT NULL,
  `type` varchar(100) NOT NULL,
  `Description` varchar(500) NOT NULL,
  `Img_Path` varchar(100) NOT NULL,
  `address` varchar(100) NOT NULL,
   `price` DECIMAL(100) NOT NULL,
    `engineCapacity` varchar(100) NOT NULL,
  PRIMARY KEY (`carid`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `passwoord` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `firstname` varchar(50) DEFAULT NULL,
  `role` varchar(50) DEFAULT 'user',
  PRIMARY KEY (`id`)
)ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

