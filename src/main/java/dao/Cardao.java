
package dao;

import beans.Cars;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import javax.persistence.Persistence;
import javax.persistence.Query;

/**
 *
 * @author YVDBL89
 */
public class Cardao {

    private static final String UNIT = "PU";
    private static EntityManagerFactory emf = Persistence.createEntityManagerFactory(UNIT);

    public void createNewCar(Cars car) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();

        em.persist(car);

        em.getTransaction().commit();
        em.close();
    }
    
        public void deleteCar(int id) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();

        Cars car = em.find(Cars.class, id);
        em.remove(car);

        em.getTransaction().commit();
        em.close();
    }

    public void editCar(Cars updatecar) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        Cars car = em.find(Cars.class, updatecar.getCarid());
        car.setCarName(updatecar.getCarName());
        car.setType(updatecar.getType());
        car.setDescription(updatecar.getDescription());
        car.setImgPath(updatecar.getImgPath());
        car.setAddress(updatecar.getAddress());
           car.setPrice(updatecar.getPrice());
           car.setEngineCapacity(updatecar.getEngineCapacity());
        em.getTransaction().commit();
        em.close();
    }

    public Cars findCarById(int id) {
        EntityManager em = emf.createEntityManager();

        Cars car = em.createNamedQuery("Cars.findByCarid", Cars.class).setParameter("carid", id).getSingleResult();

        em.close();

        return car;
    }



    public List<Cars> findAllCars() {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();

        Query query = em.createQuery("SELECT c FROM Cars c");
        List<Cars> allCars = (List<Cars>)  query.getResultList();

        em.getTransaction().commit();
        em.close();

        return allCars;
    }
     
}
