
package dao;

import beans.Login;
import beans.User;

import javax.persistence.*;

/**
 *
 * @author YVDBL89
 */
public class Logindao {

    private static final String UNIT = "PU";
    private static EntityManagerFactory emf = Persistence.createEntityManagerFactory(UNIT);
 
    
    
    
    public void registerUser(User user) {

        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.persist(user);
        em.getTransaction().commit();
        em.close();

    }

    public String authenticateUser(Login login) {
        String userName = login.getUserName();
        String password = login.getPassword();
      

        if (userName != null && password != null) {
            EntityManager em = emf.createEntityManager();
            String findByUserName = "User.findByUsername";
            
            TypedQuery<User> query = em.createNamedQuery(findByUserName, User.class);
            query.setParameter("username", userName);
            User user = query.getSingleResult();
           String userNameDB = user.getUsername();
           String passwordDB = user.getPasswoord();
        
          String  roleDB = user.getRole();

            if (userName.equals(userNameDB) && password.equals(passwordDB) && roleDB.equals("admin")) {
                return "AdminRole";
            } else if (userName.equals(userNameDB) && password.equals(passwordDB) && roleDB.equals("user")) {
                return "UserRole";
            }
        }
        return "bad credentials!";
    }

}
