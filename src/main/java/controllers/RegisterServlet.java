
package controllers;

import beans.User;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import dao.Logindao;


/**
 *
 * @author YVDBL89
 */
@WebServlet(name = "RegisterServlet", urlPatterns = {"/RegisterServlet"})
public class RegisterServlet extends HttpServlet {

   
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

 
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

   
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Logindao login = new Logindao();

        String email = request.getParameter("email");
        String name = request.getParameter("name");
     String password = request.getParameter("password");
        String username = request.getParameter("username");
        String firstName = request.getParameter("firstName");

        User user = new User();
        user.setRole("user");
        user.setEmail(email);
        user.setName(name);
        user.setPasswoord(password);
        user.setUsername(username);
        user.setFirstname(firstName);

        login.registerUser(user);

        response.sendRedirect("registered.jsp");
    }

 
}
