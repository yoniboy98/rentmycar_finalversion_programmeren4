
package controllers;

import beans.Login;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import dao.Logindao;
import javax.servlet.RequestDispatcher;

/**
 *
 * @author YVDBL89
 */
@WebServlet(name = "LoginServlet", urlPatterns = {"/LoginServlet"})
public class LoginServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String userName = request.getParameter("username");
        
        String password = request.getParameter("password");

        if (userName != null && password != null) {
            Login login = new Login();
            login.setUserName(userName);
            login.setPassword(password);

            Logindao loginService = new Logindao();

            String userValidate = loginService.authenticateUser(login);

            switch (userValidate) {
                case "AdminRole":
                    {
                  
                        HttpSession session = request.getSession();
                        session.setAttribute("Admin", userName);
                        request.setAttribute("userName", userName);
                        response.sendRedirect("AdminCarServlet");
                        break;
                    }
                case "UserRole":
                    {
                   HttpSession session = request.getSession();
                        session.setAttribute("User", userName);
                        request.setAttribute("userName", userName);
                        response.sendRedirect("UserCarServlet");
                        break;
                    }
                default:
                    System.out.println("Error message = " + userValidate);
                    request.setAttribute("errMessage", userValidate);
                    response.sendRedirect("errors.jsp");
                    break;
            }

   
        } else {
            this.getServletContext().getRequestDispatcher("/login.jsp").forward(request, response);
        }

    }
    
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        HttpSession session = request.getSession(false); 

        if (session != null) {
            session.invalidate(); 
            request.setAttribute("errMessage", "Logged out successfully");
            RequestDispatcher requestDispatcher = request.getRequestDispatcher("login.jsp");
            requestDispatcher.forward(request, response);

        }
    }

}
