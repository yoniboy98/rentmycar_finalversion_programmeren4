
package controllers;

import beans.Cars;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import dao.Cardao;

/**
 *
 * @author YVDBL89
 */
@WebServlet(name = "UserCarServlet", urlPatterns = {"/UserCarServlet"})
public class UserCarServlet extends HttpServlet {

    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {
        RequestDispatcher rd = showAllCars(request, response);
        rd.forward(request, response);

    }

    private RequestDispatcher showAllCars(HttpServletRequest request, HttpServletResponse response) throws SQLException {
        RequestDispatcher rd = request.getRequestDispatcher("user.jsp");
        Cardao cardao = new Cardao();
        List<Cars> car = cardao.findAllCars();
        request.setAttribute("car", car);
        return rd;
    }

  
   
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(UserCarServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

   
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(UserCarServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }


}
