
package controllers;

import beans.Cars;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import dao.Cardao;

/**
 *
 * @author YVDBL89
 */
@WebServlet(name = "AdminCarServlet", urlPatterns = {"/AdminCarServlet"})
public class AdminCarServlet extends HttpServlet {

    Cardao cardao = new Cardao();

  
    @Override
    public void init() throws ServletException {
        Cardao cardao = new Cardao();

    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, SQLException {
        RequestDispatcher rd = showAllCars(request, response);

     

        if (request.getParameter("deleteCarById") != null) {
            int id = Integer.parseInt(request.getParameter("id"));
            rd = deleteCarById(id, request, response);
            rd = request.getRequestDispatcher("succespage.jsp");
       
        } else if (request.getParameter("addMyCarButton") != null) {
            rd = addNewCar(request, response);
            rd = request.getRequestDispatcher("succespage.jsp");
       
        } else if (request.getParameter("editCarById") != null) {
            int id = Integer.parseInt(request.getParameter("id"));
            Cars car = cardao.findCarById(id);
            request.setAttribute("updatecar", car);
            rd = request.getRequestDispatcher("editcars.jsp");
      
        } else if (request.getParameter("editCar") != null) {
            int id = Integer.parseInt(request.getParameter("carId"));
            editCarById(id, request, response);
            rd = request.getRequestDispatcher("succespage.jsp");
        }
        else if (request.getParameter("details") != null) {
            int id = Integer.parseInt(request.getParameter("id"));
            rd = showCarDetails(id, request, response);
              request.setAttribute("car", id);
            
        }
        rd.forward(request, response);
    }

    private RequestDispatcher showAllCars(HttpServletRequest request, HttpServletResponse response) throws SQLException {
        RequestDispatcher rd = request.getRequestDispatcher("admin.jsp");
        List<Cars> car = cardao.findAllCars();
        request.setAttribute("car", car);
        return rd;
    }



    private RequestDispatcher deleteCarById(int id, HttpServletRequest request, HttpServletResponse response) throws SQLException {
        RequestDispatcher rd = request.getRequestDispatcher("admin.jsp");
        cardao.deleteCar(id);
        return rd;
    }

    private RequestDispatcher editCarById(int id, HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException {
        RequestDispatcher rd = request.getRequestDispatcher("editcars.jsp");

        String carname = request.getParameter("editname");
        String type = request.getParameter("edittype");
        String description = request.getParameter("editdescription");
        String imgpath = request.getParameter("editimage");
        String address = request.getParameter("editaddress");
         double price =Double.parseDouble(request.getParameter("editprice"));
        String ec = request.getParameter("editec");
        Cars updatedCar = new Cars(id, carname, type, description, imgpath, address,price,ec);
        cardao.editCar(updatedCar);
        return rd;
    }

     private RequestDispatcher addNewCar(HttpServletRequest request, HttpServletResponse response) throws SQLException {
         
        RequestDispatcher rd = request.getRequestDispatcher("addcars.jsp");
          String carname = request.getParameter("addname");
        String type = request.getParameter("addtype");
        String description = request.getParameter("adddescription");
        String imgpath = request.getParameter("addimage");
        String address = request.getParameter("addaddress");
        double price =Double.parseDouble(request.getParameter("addprice"));
        String ec = request.getParameter("addec");
        Cars addCar = new Cars(carname, type, description, imgpath, address,price,ec);
        cardao.createNewCar(addCar);
        return rd;
    }
     
     private RequestDispatcher showCarDetails(int id,HttpServletRequest request, HttpServletResponse response) throws SQLException {
       RequestDispatcher rd = request.getRequestDispatcher("details.jsp");
         Cars car =  cardao.findCarById(id);
        request.setAttribute("details", car);
        return rd;

    }
   
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(AdminCarServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

  
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (SQLException ex) {
            Logger.getLogger(AdminCarServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

 
 
}
