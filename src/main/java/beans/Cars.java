
package beans;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author YVDBL89
 */
@Entity
@Table(name = "cars", catalog = "mycar", schema = "")
@NamedQueries({
    @NamedQuery(name = "Cars.findAll", query = "SELECT c FROM Cars c"),
    @NamedQuery(name = "Cars.findByCarid", query = "SELECT c FROM Cars c WHERE c.carid = :carid"),
    @NamedQuery(name = "Cars.findByCarName", query = "SELECT c FROM Cars c WHERE c.carName = :carName"),
    @NamedQuery(name = "Cars.findByType", query = "SELECT c FROM Cars c WHERE c.type = :type"),
    @NamedQuery(name = "Cars.findByDescription", query = "SELECT c FROM Cars c WHERE c.description = :description"),
    @NamedQuery(name = "Cars.findByImgPath", query = "SELECT c FROM Cars c WHERE c.imgPath = :imgPath"),
      @NamedQuery(name = "Cars.findByEngineCapacity", query = "SELECT c FROM Cars c WHERE c.engineCapacity = :engineCapacity"),
        @NamedQuery(name = "Cars.findByPrice", query = "SELECT c FROM Cars c WHERE c.price = :price"),
    @NamedQuery(name = "Cars.findByAddress", query = "SELECT c FROM Cars c WHERE c.address = :address")})
public class Cars implements Serializable {

  
  

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "carid")
    private Integer carid;
  @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "carName")
    private String carName;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "type")
    private String type;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 500)
    @Column(name = "Description")
    private String description;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "Img_Path")
    private String imgPath;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "address")
    private String address;
      @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10000)
    @Column(name = "price")
    private double price;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 100)
    @Column(name = "engineCapacity")
    private String engineCapacity;


    public Cars() {
    }

    public Cars(Integer carid) {
        this.carid = carid;
    }

    public Cars(Integer carid, String carName, String type, String description, String imgPath, String address , double price, String ec) {
        this.carid = carid;
        this.carName = carName;
        this.type = type;
        this.description = description;
        this.imgPath = imgPath;
        this.address = address;
        this.price= price;
        this.engineCapacity= ec;
    }

    public Cars(String carName, String type, String description, String imgPath, String address,double price, String ec) {
         this.carName = carName;
        this.type = type;
        this.description = description;
        this.imgPath =  imgPath;
        this.address = address;
            this.price= price;
        this.engineCapacity= ec;
    }




    public Integer getCarid() {
        return carid;
    }

    public void setCarid(Integer carid) {
        this.carid = carid;
    }

    public String getCarName() {
        return carName;
    }

    public void setCarName(String carName) {
        this.carName = carName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImgPath() {
        return imgPath;
    }

    public void setImgPath(String imgPath) {
        this.imgPath = imgPath;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
     public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getEngineCapacity() {
        return engineCapacity;
    }

    public void setEngineCapacity(String engineCapacity) {
        this.engineCapacity = engineCapacity;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (carid != null ? carid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Cars)) {
            return false;
        }
        Cars other = (Cars) object;
        if ((this.carid == null && other.carid != null) || (this.carid != null && !this.carid.equals(other.carid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "beans.Cars[ carid=" + carid + " ]";
    }

   

 
    
}
