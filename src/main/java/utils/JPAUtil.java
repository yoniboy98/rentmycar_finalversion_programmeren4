

/**
 *
 * @author YVDBL89
 */

package utils;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;


public class JPAUtil{
    private final EntityManagerFactory emf;
    private static JPAUtil jpaUtility;


    public void close() {
        emf.close();
    }

    public EntityManager createEntityManager() {
        return emf.createEntityManager();
    }

    public JPAUtil() {
        emf = Persistence.createEntityManagerFactory("PU");
    }

    public static JPAUtil getInstance() {
        if (jpaUtility == null) {
            jpaUtility = new JPAUtil();
        }
        return jpaUtility;
    }
}


 