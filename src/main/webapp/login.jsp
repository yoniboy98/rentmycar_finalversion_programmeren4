<%-- 
    Document   : login
    Created on : Mar 11, 2020, 9:05:05 AM
    Author     : YVDBL89
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Login </title>
        <link rel="stylesheet" type ="text/css" href="design/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="design/style.css">
    </head>
    <body>
        <form name="form" action="<%=request.getContextPath()%>/LoginServlet" method="post">
            <center> <h1><span class="ez-toc-section">Login</span></h1> </center>
            <table align="center">
 
     
     <div class="alert alert-warning" role="alert">
  
      <span style="color:black"><%=(request.getAttribute("errMessage") == null) ? "" : request.getAttribute("errMessage")%></span></td>
     </div>
     
                <tr>
                <div class="form-group">
                    <td>Username: </td>
                    <td><input class="form-control" type="text" name="username" placeholder ="enter username "required/></td>
                </div>
                </tr>
                <tr>
                <div class="form-group">
                    <td>Password: </td>
                    <td><input class="form-control" type="password" name="password" placeholder ="enter password"required/></td>
                </div>
                </tr>
               
                <tr>
                    <td></td>
                    <td><input style="background-color: white; "class="form-control btn btn-login btn-default" type="submit" value="Login"></input>
                <center><a style=" " href="register.jsp">Create an account</a></center></td>
                </tr>
            </table>
        </form>
    </body>
</html>
