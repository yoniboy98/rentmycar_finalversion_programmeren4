<%-- 
    Document   : register
    Created on : Mar 11, 2020, 1:26:10 PM
    Author     : YVDBL89
--%>

<%@page import="beans.User" contentType="text/html" pageEncoding="UTF-8"%>    
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type ="text/css" href="design/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="design/style.css">
        <title>Register Page</title>
    </head>
    <body>
    <body>


        <% User user = new User();%>

        <form action="<%= request.getContextPath()%>/RegisterServlet" method="post">
            <center> <h1><span class="ez-toc-section">Register here</span></h1> </center>
            <table align="center" style="with: 80%;">

                <tr>
                <div class="form-group">
                    <td>First Name:  </td>
                    <td>
                        <input  class="form-control" type="text" name="firstName" placeholder="enter first name" required/></td><td>
                </div>
                </tr>
                <tr>
                <div class="form-group">
                    <td>Last Name:  </td>
                    <td><input  class="form-control" type="text" name="name" placeholder="enter family name" required/></td><td>
                </div>
                </tr>
                <tr>
                <div class="form-group">
                    <td>UserName:  </td>
                    <td><input  class="form-control" type="text" name="username" placeholder="enter username" required/></td>
                </div>
                </tr>
                <tr>
                <div class="form-group">
                    <td>Password:  </td>
                    <td><input  class="form-control" type="password" name="password" placeholder="enter password"required/></td>
                </div>
                </tr>
                <tr>
                <div class="form-group">
                    <td>Email:  </td>
                    <td><input  class="form-control" type="email" name="email" placeholder="enter email" /></td>
                </div>
                </tr>

                <td><center> <input style="background-color: white;" class="form-control btn btn-register btn-default" type = "submit" value = "Register" /></center> </td>
                </div>


        </form>


    </div>
</body>
</html>

