<%-- 
    Document   : admin
    Created on : Mar 12, 2020, 9:46:34 AM
    Author     : YVDBL89
--%>

<%@page import="beans.Cars"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Admin car Page</title>
        <link rel="stylesheet" type ="text/css" href="design/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="design/style.css">
    </head>
    <body>
        <div class="container">
            <nav class ="navbar navbar-expand-md navbar fixed-top navbar-light bg-light " role = "navigation">
                <a class="navbar-brand" href="#">RentMyCar</a>
                <ul class="navbar-nav">
                    <li class="active nav-item">
                        <a class="nav-link" href="<%=request.getContextPath()%>/AdminCarServlet">Home</a>
                    </li>
                
                    <li class="nav-item">
                        <a class="nav-link" href="#">Rent a car now</a>
                    </li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
             
                 
                    <li class="nav-item"><a class="nav-link" href="<%=request.getContextPath()%>/LoginServlet"><span class="glyphicon glyphicon-log-in"></span> Logout</a></li>
                </ul>
            </nav>
        </div>

        <br>
        <br>
        <h1 style="text-align: center" >  Cars to rent !  </h1>
         
        
        <div class="container">
            <button><center><a style=" color: blue;" href="addcars.jsp" name="addcars">Add new car</a></center></button>
                                </div> 
        
        
        
        
        <%
            List<Cars> car = (List<Cars>) request.getAttribute("car");
            for (Cars c : car) {
        %> 
        
           
                                
                             
                        
        <div class="container">
            <div class ="row">
              
                    <table class="table table-striped">
                       
                         
                     
                        <tr>
                            <td>
                                <h3 style="text-align: center">name: <%=c.getCarName()%></h3>
               <p><img class="img-responsive" src="IMAGES\<%=c.getImgPath()%>" alt="<%=c.getImgPath()%>"></p>
                          <p><b>type: </b><%=c.getType()%></p>
                                <p><b>description: </b><%=c.getDescription()%></p>
                                 <p><b>price: </b><%=c.getPrice()%> euro /day</p>
                               
                      
                            </td>
                        </tr>
                        <tr> 
                            <td>
                                
                                
                        <center>
                            <a href="AdminCarServlet?deleteCarById&id=<%=c.getCarid()%>" name="delete"> Delete </a>
                            <a href="AdminCarServlet?editCarById&id=<%=c.getCarid()%>" name="edit"> Edit</a> 
                              <a href="AdminCarServlet?details&id=<%=c.getCarid()%>" type="submit" name="details"> details</a> 
                        </center>
                        </td>
                        <%
                            }
                        %>
                       
                    </table>
                 
            </div>
        </div>






    </body>
</html>
