<%-- 
    Document   : user
    Created on : Mar 15, 2020, 9:46:28 AM
    Author     : YVDBL89
--%>

<%@page import="beans.Cars"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type ="text/css" href="design/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="design/style.css">
        <title>Homepage</title>
    </head>
    <div class="container">
        <nav class ="navbar navbar-expand-md navbar fixed-top navbar-light bg-light " role = "navigation">
            <a class="navbar-brand" href="#">RentMyCar</a>
            <ul class="navbar-nav">
                <li class="active nav-item">
                    <a class="nav-link" href="<%=request.getContextPath()%>/UserCarServlet">Home</a>
                </li>
               
                <li class="nav-item">
                    <a class="nav-link" href="#">Rent a car now</a>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
            <li class="nav-item">
                    <a class="nav-link" href="bron.jsp">citation</a>
                </li>
          
                <li class="nav-item"><a class="nav-link" href="<%=request.getContextPath()%>/LoginServlet"><span class="glyphicon glyphicon-log-in"></span> Logout</a></li>
      
            
                      </ul>
        </nav>
    </div>


    <br>
    <br>
    <u><h1 style="text-align: center"> All cars </h1></u>
    <br>
    <%
        List<Cars> car = (List<Cars>) request.getAttribute("car");
        for (Cars c : car) {
    %> 
    <div class="container">
        <div class ="row">
            <div class="col">
                <table class="table table-striped">
                    <tr>
                        <td>
                            <h3 style="text-align: center">name: <%=c.getCarName()%></h3>
                                  <p><img class="img-responsive" src="IMAGES\<%=c.getImgPath()%>" alt="<%=c.getImgPath()%>"></p>
                            <p><b>type: </b> <%=c.getType()%></p>
                            <p><b>description: </b><%=c.getDescription()%></p>
                            <p><b>address: </b><%=c.getAddress()%></p>
                      
                        </td>
                    </tr>
                    <tr> 
                        <td>
                            <button><a href="AdminCarServlet?details&id=<%=c.getCarid()%>" type="submit" name="details"> details</a> </button>
                        </td>
                        <%
                            }
                        %>

                </table>
               
        </div>
    </div>






</body>

</html>
