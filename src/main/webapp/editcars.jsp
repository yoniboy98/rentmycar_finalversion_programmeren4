<%-- 
    Document   : editcars
    Created on : Mar 15, 2020, 2:55:12 PM
    Author     : YVDBL89
--%>

<%@page import="beans.Cars"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <% Cars updatecar = (Cars) request.getAttribute("updatecar");
        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Edit Car</title>
        <link rel="stylesheet" type ="text/css" href="design/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="design/style.css">
    </head>
    <body>
        <div class="container">
            <h1>Edit Car</h1>
            <form name="editcar" action="AdminCarServlet?ed">
                <input type="hidden" value="<%=updatecar.getCarid()%>"name="carId" placeholder="id" size="5">
                <div class="form-group">
                    <p>name <input class="form-control" type="text" value="<%=updatecar.getCarName()%>"name="editname" placeholder="name" size="50"></p>
                </div>
                <div class="form-group">
                    <p>type <input class="form-control" type="text" value="<%=updatecar.getType()%>"name="edittype" placeholder="type" size="50"></p>
                </div>
                <div class="form-group">
                    <p>Description: <input class="form-control" type="text" value="<%=updatecar.getDescription()%>" name="editdescription" placeholder="Description" size="50"></p>
                </div>
                
                <div class="form-group">
                    <p>address: <input class="form-control" type="text" value="<%=updatecar.getAddress()%>"name="editaddress" placeholder="address" size="50"></p>
                </div>
                 <div class="form-group">
                    <p>price: <input class="form-control" type="number" value="<%=updatecar.getPrice()%>"name="editprice" placeholder="price" size="50"></p>
                </div>
                 <div class="form-group">
                    <p>engine capacity:<input class="form-control" type="text" value="<%=updatecar.getEngineCapacity()%>"name="editec" placeholder="engine" size="50"></p>
                </div>
                <div class="form-group">
                    <p> image: <input class="form-control" type="text" value="<%=updatecar.getImgPath()%>"name="editimage" placeholder="image(bmw.jpg)" size="50"></p>
                </div>
                <button class="btn btn-primary" type="submit" value="submit" name="editCar">Edit Car</button>
            </form>
        </div>
    </body>
</html>
