<%-- 
    Document   : addCar
    Created on : Mar 17, 2020, 1:35:29 PM
    Author     : YVDBL89
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Add new car</title>
        <link rel="stylesheet" type ="text/css" href="design/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="design/style.css">
    </head>
    <body>
        <div class="container">
            <center><h1>Add a new car to rent:</h1></center>
            <form name="addCarForm" action="AdminCarServlet?">
              
                
                <div class="form-group">
                    <p>name: <input class="form-control" type="text" name="addname" placeholder="name" size="50" required></p>
                </div>
                    
                    <div class="form-group">
                        <p>type: <input class="form-control" type="text" name="addtype" placeholder="type" size="50"required></p>
                    </div>
                        
                        <div class="form-group">
                            <p>Description: <input class="form-control" type="text" name="adddescription" placeholder="Description" size="50"></p>
                        </div>
                            
                            <div class="form-group">
                                    <p>address: <input class="form-control" type="text" name="addaddress" placeholder="address" size="50"required></p>
                                    </div>
                
                 <div class="form-group">
                                    <p>price: <input class="form-control" type="number" name="addprice" placeholder="price" size="50"required></p>
                                    </div>
                
                 <div class="form-group">
                                    <p>engine capacity: <input class="form-control" type="text" name="addec" placeholder="engine capacity" size="50"required></p>
                                    </div>
                                    
                                    <div class="form-group">
                                <p> image: <input class="form-control" type="text" name="addimage" placeholder=" image(bmw.jpg)" size="50"required></p>
                                     </div>
                                
                                
                                <div class="form-group">
                                    <button class="btn btn-primary" type="submit" value="submit" name="addMyCarButton">Save car</button>
                                    </form>
                                </div>
                                </body>
                                </html>
